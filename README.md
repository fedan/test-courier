## Courier service
### Install project
 
## Install project 
 1. cd procject_dir.
 2. configure DB connection in .env
 3. run composer install
 4. php bin/console doctrine:database:create
 5. php bin/console doctrine:schema:create
 
##### You can run fixtures and create fake data
 * php bin/console doctrine:fixtures:load
 
##### API doc
 * [doc](https://docs.google.com/spreadsheets/d/1WdSuenS8BFw78YF2Pr1tQwFFhJ3tbHEurjGpcLsHuTo/edit?usp=sharing)