<?php

namespace App\Normalizer;

/**
 * Class AddressNormalizer
 * @package App\Normalizer
 */
class AddressNormalizer implements NormalizerInterface
{
    /**
     * @param $entity
     * @return array
     */
    public function normalize($entity)
    {
        return [
            'id' => $entity->getId(),
            'country' => $entity->getCountry(),
            'city' => $entity->getCity(),
            'street' => $entity->getStreet(),
            'building' => $entity->getBuilding(),
            'flat' => $entity->getFlat()
        ];
    }
}
