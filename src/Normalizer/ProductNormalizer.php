<?php

namespace App\Normalizer;

/**
 * Class ProductNormalizer
 * @package App\Normalizer
 */
class ProductNormalizer implements NormalizerInterface
{
    /**
     * @param $entity
     * @return array
     */
    public function normalize($entity)
    {
        return [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'prise' => $entity->getPrice(),
            'addressStart' => (new AddressNormalizer())->normalize($entity->getAddressStart()),
            'addressEnd' => (new AddressNormalizer())->normalize($entity->getAddressEnd()),
        ];
    }
}
