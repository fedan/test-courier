<?php

namespace App\Normalizer;

/**
 * Class MoneyTransactionNormalizer
 * @package App\Normalizer
 */
class MoneyTransactionNormalizer implements NormalizerInterface
{
    public function normalize($entity)
    {
        return [
            'id' => $entity->getId(),
            'date' => $entity->getDate()->format('d-m-Y h:m:s'),
            'cashier' => (new WorkerNormalizer())->normalize($entity->getCashier()),
            'courier' => (new WorkerNormalizer())->normalize($entity->getCourier()),
            'amount' => $entity->getAmount()
        ];
    }
}
