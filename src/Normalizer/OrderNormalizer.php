<?php

namespace App\Normalizer;

/**
 * Class OrderNormalizer
 * @package App\Normalizer
 */
class OrderNormalizer implements NormalizerInterface
{
    public function normalize($entity)
    {
        return [
            'id' => $entity->getId(),
            'number' => $entity->getNumber(),
            'status' => $entity->getStatus(),
            'product' => (new ProductNormalizer())->normalize($entity->getProduct()),
            'courier' => (new WorkerNormalizer())->normalize($entity->getCourier()),
        ];
    }
}
