<?php

namespace App\Normalizer;

/**
 * Class WorkerNormalizer
 * @package App\Normalizer
 */
class WorkerNormalizer implements NormalizerInterface
{
    /**
     * @param $entity
     * @return array
     */
    public function normalize($entity)
    {
        return [
            'id' => $entity->getId(),
            'firstName' => $entity->getFirstName(),
            'lastName' => $entity->getLastName(),
            'type' => $entity->getType(),
        ];
    }
}
