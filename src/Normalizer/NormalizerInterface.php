<?php

namespace App\Normalizer;

/**
 * Interface NormalizerInterface
 * @package App\Serializer
 */
interface NormalizerInterface
{
    public function normalize($entity);
}
