<?php

namespace App\Repository;

use App\Entity\Ordering;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ordering|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ordering|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ordering[]    findAll()
 * @method Ordering[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    /**
     * OrderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ordering::class);
    }

    /**
     * Get courier's orders
     * @param $id
     * @return mixed
     */
    public function getCourierOrders($courier)
    {
        return $this->createQueryBuilder('o')
            ->where('o.status != :status ')
            ->andWhere('o.courier = :courier')
            ->setParameter('status', 'finished')
            ->setParameter('courier', $courier)
            ->getQuery()
            ->getResult();
    }

    /**
     * Get courier's order
     *
     * @param $courier
     * @param $taskId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCourierOrder($courier, $taskId)
    {
        return $this->createQueryBuilder('o')
            ->where('o.status != :status ')
            ->andWhere('o.courier = :courier')
            ->andWhere('o.id = :id')
            ->setParameter('id', $taskId)
            ->setParameter('status', 'finished')
            ->setParameter('courier', $courier)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
