<?php


namespace App\Repository;


use App\Entity\MoneyTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class MoneyTransactionRepository
 * @package App\Repository
 */
class MoneyTransactionRepository extends ServiceEntityRepository
{
    /**
     * MoneyTransactionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoneyTransaction::class);
    }
}
