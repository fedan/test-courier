<?php

namespace App\Controller;

use App\Entity\MoneyTransaction;
use App\Entity\Worker;
use App\Normalizer\MoneyTransactionNormalizer;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MoneyTransactionController
 * @package App\Controller
 * @Route("/api")
 */
class MoneyTransactionController extends ApiController
{
    const COURIER = 2;
    const CASHIER = 1;

    /**
     * Give money back task
     *
     * @Route("/courier/{id}/give-money-back", name="courier-give-money-back", methods={"POST"})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function giveMoneyBack($id)
    {
        $em = $this->getDoctrine()->getManager();
        $cashier = $em->getRepository(Worker::class)->find(self::CASHIER);

        if (!$courier = $em->getRepository(Worker::class)->find($id)) {
            return $this->json('Worker not found', 404);
        }

        if (!$courier->getAccount() and $courier->getType() == self::COURIER) {
            return $this->json('Forbidden', 403);
        }

        $moneyTransaction = new MoneyTransaction();
        $moneyTransaction->setCashier($cashier);
        $moneyTransaction->setCourier($courier);
        $moneyTransaction->setAmount($courier->getAccount());
        $em->persist($moneyTransaction);

        $cashier->setAccount($cashier->getAccount()+$courier->getAccount());
        $courier->setAccount(0);
        $em->flush();

        return $this->json((new MoneyTransactionNormalizer())->normalize($moneyTransaction));
    }

    /**
     * Get list transactions
     *
     * @Route("/transactions", name="transactions", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listTransaction()
    {
        $em = $this->getDoctrine()->getManager();
        if (!$moneyTransactions = $em->getRepository(MoneyTransaction::class)->findAll()) {
            return $this->json('Data not found', 404);
        }
        $result = [];
        foreach ($moneyTransactions as $moneyTransaction) {
            $result['moneyTransaction'][] = (new MoneyTransactionNormalizer())->normalize($moneyTransaction);
        }

        return $this->json($result);
    }

    /**
     * Get transaction
     *
     * @Route("/transaction/{id}", name="transaction", methods={"GET"})
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTransaction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$moneyTransaction = $em->getRepository(MoneyTransaction::class)->find($id)) {
            return $this->json('Data not found', 404);
        }

        return $this->json((new MoneyTransactionNormalizer())->normalize($moneyTransaction));
    }
}
