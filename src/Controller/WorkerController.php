<?php

namespace App\Controller;

use App\Entity\Ordering;
use App\Entity\Worker;
use App\Normalizer\OrderNormalizer;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WorkerController
 * @package App\Controller
 * @Route("/api")
 */
class WorkerController extends ApiController
{
    /**
     *  Get tasks list
     *
     * @Route("/courier/{id}/tasks", name="worker-tasks", methods={"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listTasks($id)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$courier = $em->getRepository(Worker::class)->find($id)) {
            return $this->json('Worker not found', 404);
        }

        $orders = $em->getRepository(Ordering::class)->getCourierOrders($courier);
        $result = [];
        foreach ($orders as $order) {
            $result['orders'][] = (new OrderNormalizer())->normalize($order);
        }
        return $this->json($result);
    }

    /**
     * Start task
     *
     * @Route("/courier/{id}/task/{task}/start", name="worker-task-start", methods={"PUT"})
     *
     * @param $id
     * @param $task
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function startTask($id, $task)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$courier = $em->getRepository(Worker::class)->find($id)) {
            return $this->json('Worker not found', 404);
        }
        /** @var Ordering $order */
        if (!$order = $em->getRepository(Ordering::class)->getCourierOrder($courier, $task)) {
            return $this->json('Task not found', 400);
        }

        if ($order->getStatus() != 'new') {
            return $this->json('Forbidden', 403);
        }

        $order->setStatus('started');
        $em->flush();
        return $this->json((new OrderNormalizer())->normalize($order));
    }

    /**
     * Finish task
     *
     * @Route("/courier/{id}/task/{task}/finish", name="worker-task-end", methods={"PUT"})
     *
     * @param $id
     * @param $task
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function finishTask($id, $task)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$courier = $em->getRepository(Worker::class)->find($id)) {
            return $this->json('Worker not found', 404);
        }

        if (!$order = $em->getRepository(Ordering::class)->getCourierOrder($courier, $task)) {
            return $this->json('Task not found', 400);
        }

        if ($order->getStatus() != 'started') {
            return $this->json('Forbidden', 403);
        }

        $order->setStatus('finished');
        $courier->setAccount($order->getProduct()->getPrice());
        $em->flush();
        return $this->json((new OrderNormalizer())->normalize($order));
    }
}
