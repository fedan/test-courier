<?php

namespace App\Controller;

use App\Entity\Ordering;
use App\Form\OrderType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderController
 *
 * @package App\Controller
 * @Route("/api")
 */
class OrderController extends ApiController
{
    const CASHIER = 1;

    /**
     * Create new order
     *
     * @Route("/order", name="order-create", methods={"POST"})
     * @param Request $request
     * @param EntityManager $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $order = new Ordering();
        $form = $this->createForm(OrderType::class, $order);
        $data = json_decode($request->getContent(), 1);
        $form->submit($data);

        if (!$form->isValid()) {
            return $this->json($this->getErrorsFromForm($form), 400);
        }

        $order->setNumber(time());
        $order->setStatus('new');
        $em->persist($order);
        $em->flush();

        return $this->json(['orderId' => $order->getId()], 201);
    }
}
