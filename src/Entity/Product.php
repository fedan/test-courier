<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $price;

    /**
     * @ManyToOne(targetEntity="Address", inversedBy="product", cascade={"persist"})
     * @JoinColumn(name="address_start_id", referencedColumnName="id")
     */
    private $addressStart;

    /**
     * @ManyToOne(targetEntity="Address", inversedBy="product", cascade={"persist"})
     * @JoinColumn(name="address_end_id", referencedColumnName="id")
     */
    private $addressEnd;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getAddressStart()
    {
        return $this->addressStart;
    }

    /**
     * @param mixed $addressStart
     */
    public function setAddressStart($addressStart): void
    {
        $this->addressStart = $addressStart;
    }

    /**
     * @return mixed
     */
    public function getAddressEnd()
    {
        return $this->addressEnd;
    }

    /**
     * @param mixed $addressEnd
     */
    public function setAddressEnd($addressEnd): void
    {
        $this->addressEnd = $addressEnd;
    }
}
