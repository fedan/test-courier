<?php

namespace App\DataFixtures;

use App\Entity\Worker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class WorkerFixture
 * @package App\DataFixtures
 */
class WorkerFixture extends Fixture
{
    const COURIER = 2;
    const CASHIER = 1;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 5; $i++) {
            $worker = new Worker();
            $worker->setFirstName($faker->firstName);
            $worker->setLastName($faker->lastName);
            $worker->setType(self::COURIER);

            if ($i == 1) {
                $worker->setType(self::CASHIER);
            }
            $manager->persist($worker);
        }
        $manager->flush();
    }
}
