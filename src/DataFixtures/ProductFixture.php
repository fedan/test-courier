<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class CustomerFixture
 * @package App\DataFixtures
 */
class ProductFixture extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 20; $i++) {
            $product = new Product();
            $product->setName('Product-' . $faker->numberBetween(1, 50));
            $product->setPrice($faker->numberBetween(10, 1000));
            $product->setAddressStart($this->createAddress());
            $product->setAddressEnd($this->createAddress());

            $manager->persist($product);
        }
        $manager->flush();
    }

    protected function createAddress()
    {
        $faker = \Faker\Factory::create();

        $address = new Address();
        $address->setCountry('Ukraine');
        $address->setCity('Lviv');
        $address->setBuilding($faker->buildingNumber);
        $address->setFlat($faker->numberBetween(1, 50));
        $address->setStreet($faker->streetName);

        return $address;
    }
}
