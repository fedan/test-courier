<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AddressFixture
 * @package App\DataFixtures
 */
class AddressFixture extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i < 10; $i++) {
            $address = new Address();
            $address->setCountry('Ukraine');
            $address->setCity('Lviv');
            $address->setBuilding($faker->buildingNumber);
            $address->setFlat($faker->numberBetween(1, 50));
            $address->setStreet($faker->streetName);

            $manager->persist($address);

        }
        $manager->flush();
    }
}
