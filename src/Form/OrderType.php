<?php

namespace App\Form;

use App\Entity\Customer;
use App\Entity\Ordering;
use App\Entity\Product;
use App\Entity\Worker;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderType
 * @package App\Form
 */
class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add('status')
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'required' => true
            ])
            ->add('customer', EntityType::class, [
                'class' => Customer::class,
                'required' => true
            ])
            ->add('courier', EntityType::class, [
                'class' => Worker::class,
                'required' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ordering::class,
        ]);
    }
}
